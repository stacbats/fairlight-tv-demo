
BasicUpstart2(main)


main:	
    lda #72
    jsr $ffD2
    lda #73
    jsr $ffd2

    sei

	lda #$7f
	sta $dc0d 	// Turn off CIA#1
	sta $dd0d 	// Turn off CIA#2

	lda #$01 
	sta $d01a 	// Turn on Raster interrupt
	asl $d019 	// Clear pending IRQ

	lda #$f2	// Set the IRQ to ro $fb
	sta $d012
	lda $d011 	// Clear the MSB of raster
	and #$7f 
	sta $d011

	lda #<IRQ
	sta $0314 
	lda #>IRQ 
	sta $0315 
	cli

	rts
	//jmp *-3

IRQ:	inc $d020
	ldx #$ff 
!:	nop
	dex
	bne !-
	dec $d020
	asl $d019
	jmp $ea31
	//jmp $ea81
